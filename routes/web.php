<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/posts/{post}', ['as'=>'home.post', 'uses'=>'HomeController@post']);

Route::group(['middleware'=>'admin'], function(){
	// Route::get('/admin', ['as'=>'admin.index'], function(){
	// 	return view('admin.index');
	// });
	Route::get('/admin', ['as'=>'admin.index', 'uses'=>'AdminController@index']);

	Route::resource('admin/users', 'AdminUsersController');
	
	Route::resource('admin/posts', 'AdminPostsController');
	
	Route::resource('admin/categories', 'AdminCategoriesController');
	
	Route::resource('admin/photos', 'AdminPhotosController');
	
	Route::delete('admin/delete/photos', 'AdminPhotosController@deletePhotos');
	
	Route::get('admin/comments', 'AdminCommentsController@index')->name('comments.index');
	Route::get('admin/comments/{comment}', 'AdminCommentsController@show')->name('comments.show');
	Route::patch('admin/comments/{comment}', 'AdminCommentsController@update')->name('comments.update');
	Route::delete('admin/comments/{comment}', 'AdminCommentsController@destroy')->name('comments.destroy');

	
	Route::get('admin/comment/replies/{reply}', 'AdminCommentRepliesController@show')->name('replies.show');
	Route::patch('admin/comment/replies/{reply}', 'AdminCommentRepliesController@update')->name('replies.update');
	Route::delete('admin/comment/replies/{reply}', 'AdminCommentRepliesController@destroy')->name('replies.destroy');
	
	Route::get('/profile', ['as'=>'home.profile', 'uses'=>'HomeController@profile']);
});

Route::group(['middleware'=>'auth'], function(){
    Route::post('admin/comment/replies', 'AdminCommentRepliesController@store')->name('replies.store');
    Route::post('admin/comments', 'AdminCommentsController@store')->name('comments.store');
});

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
	\UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::fallback(function(){
    return view('errors.404');
});

// Route::resource('admin/posts', 'AdminPostsController');

// Route::resource('admin/categories', 'AdminCategoriesController');
