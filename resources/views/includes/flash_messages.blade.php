<!-- Users -->
@if(Session::has('success_create_user_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_create_user_message')}}</p>
	</div>

@endif

@if(Session::has('success_update_user_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_update_user_message')}}</p>
	</div>

@endif

@if(Session::has('success_delete_user_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_delete_user_message')}}</p>
	</div>

@endif

<!-- Posts -->
@if(Session::has('info_create_category_first_message'))
	
	<div class="alert alert-info col-sm-6">
    	<p class="text-center">{{session('info_create_category_first_message')}}</p>
	</div>

@endif

@if(Session::has('success_create_post_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_create_post_message')}}</p>
	</div>

@endif

@if(Session::has('success_update_post_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_update_post_message')}}</p>
	</div>

@endif

@if(Session::has('success_delete_post_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_delete_post_message')}}</p>
	</div>

@endif

<!-- Comments -->
@if(Session::has('success_comment_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_comment_message')}}</p>
	</div>

@endif

@if(Session::has('success_comment_approved_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_comment_approved_message')}}</p>
	</div>

@endif

@if(Session::has('success_comment_unapproved_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_comment_unapproved_message')}}</p>
	</div>

@endif

@if(Session::has('success_delete_comment_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_delete_comment_message')}}</p>
	</div>

@endif

<!-- Replies -->
@if(Session::has('success_reply_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_reply_message')}}</p>
	</div>

@endif

@if(Session::has('success_reply_approved_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_reply_approved_message')}}</p>
	</div>

@endif

@if(Session::has('success_reply_unapproved_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_reply_unapproved_message')}}</p>
	</div>

@endif

@if(Session::has('success_delete_reply_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_delete_reply_message')}}</p>
	</div>

@endif

<!-- Categories -->
@if(Session::has('success_create_category_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_create_category_message')}}</p>
	</div>

@endif

@if(Session::has('success_update_category_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_update_category_message')}}</p>
	</div>

@endif

@if(Session::has('success_delete_category_message'))
	
	<div class="alert alert-success col-sm-6">
    	<p class="text-center">{{session('success_delete_category_message')}}</p>
	</div>

@endif

<!-- Photos -->
@if(Session::has('success_delete_photos_message'))
	
	<div class="alert alert-success col-sm-6">
		<p class="text-center">{{session('success_delete_photos_message')}}</p>
	</div>

@endif

@if(Session::has('success_delete_photo_message'))
	
	<div class="alert alert-success col-sm-6">
		<p class="text-center">{{session('success_delete_photo_message')}}</p>
	</div>

@endif

