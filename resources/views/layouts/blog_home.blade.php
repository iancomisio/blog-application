<!-- Header -->
@include('includes.home_header')

<!-- Navigation -->
@include('includes.home_nav')

<!-- Page Content -->
@include('includes.flash_messages')

@yield('content')

<!-- Footer -->
@include('includes.home_footer')



