@extends('layouts.blog_home')

@section('content')

<div class="row">
    <div class="col-sm-6">
        @include('includes.form_error')
    </div>
</div>

<div class="row">
    <div class="col-md-8">
    <!-- Blog Post -->

    <!-- Title -->
        <h1><a href="{{route('home.post', $post->slug)}}">{{$post->title}}</a></h1>

        <!-- Author -->
        <p class="lead">
            by {{$post->user->name}}
        </p>

        <hr>

        <hr>

        <!-- Date/Time -->
        <p><span class="glyphicon glyphicon-time"></span> Posted {{$post->created_at->diffForHumans()}}</p>

        <hr>
        <!-- Preview Image -->
        <img class="img-responsive" src="{{$post->photo ? $post->photo->file: $post->photoPlaceholder()}}" alt="">
        <hr>

        <!-- Post Content -->

        <p>{!!$post->body!!}</p>

        <hr>

        Custom Comments and Reply Section

        <br>
        <br>

        

        <!-- Blog Comments -->

        @if(Auth::check())

            <!-- Comments Form -->
            <div class="well">
                <h4>Leave a Comment:</h4>


                {!! Form::open(['method'=>'POST', 'action'=> 'AdminCommentsController@store']) !!}
                    <input type="hidden" name="post_id" value="{{$post->id}}">


                    <div class="form-group">
                        {!! Form::label('body', 'Body:') !!}
                        {!! Form::textarea('body', null, ['class'=>'form-control','rows'=>3])!!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Submit Comment', ['class'=>'btn btn-primary']) !!}
                    </div>
                {!! Form::close() !!}
            
            </div>

        @endif

            <hr>

        <!-- Posted Comments -->

        @if(count($comments) > 0)


            @foreach($comments as $comment)

                <!-- Comment -->
                <div class="media">
                    <a class="pull-left" href="#">
                        {{-- <img height="64" class="media-object" src="{{Auth::user()->gravatar}}" alt=""> --}}
                        <img height="64" class="media-object" src="{{$comment->photo ? : 'http://placehold.it/400x400' }}" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">{{$comment->author}}
                            <small>{{$comment->created_at->diffForHumans()}}</small>
                        </h4>
                        <p>{{$comment->body}}</p>

                        @if(count($comment->replies) > 0)

                            @foreach($comment->replies as $reply)

                                @if($reply->is_active == 1)

                                    <!-- Nested Comment -->
                                    <div id="nested-comment" class=" media">
                                        <a class="pull-left" href="#">
                                            <img height="64" class="media-object" src="{{$reply->photo ? : 'http://placehold.it/400x400' }}" alt="">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading">{{$reply->author}}
                                                <small>{{$reply->created_at->diffForHumans()}}</small>
                                            </h4>
                                            <p>{{$reply->body}}</p>
                                        </div>

                                        
                                    <!-- End Nested Comment -->
                                    </div>

                                {{-- @else

                                    <h1 class="text-center">No Replies</h1> --}}

                                @endif

                            @endforeach

                        @endif

                        <div class="comment-reply-container">

                            <button class="toggle-reply btn btn-primary pull-right">Reply</button>

                            <div class="comment-reply col-sm-6">

                                {!! Form::open(['method'=>'POST', 'action'=> 'AdminCommentRepliesController@store']) !!}
                                <div class="form-group">
                                    <br>
                                    <br>
                                    <input type="hidden" name="comment_id" value="{{$comment->id}}">
                                    {!! Form::label('body', 'Body:') !!}
                                    {!! Form::textarea('body', null, ['class'=>'form-control','rows'=>1])!!}
                                </div>

                                <div class="form-group">
                                    {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        
                    </div>
                </div>

            @endforeach
        
        @endif

    </div> <!-- col-md-8-->
    @include('includes.home_sidebar')
</div> <!-- Row -->


@endsection


@section('scripts')

    <script>

        $(".comment-reply-container .toggle-reply").click(function(){
            $(this).next().slideToggle("slow");
        });

    </script> 

{{-- <div id="disqus_thread"></div>
    <script>

    /**
    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
    /*
    var disqus_config = function () {
    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    */
    (function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = 'https://blog-app-4.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
    })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

    <script id="dsq-count-scr" src="//blog-app-4.disqus.com/count.js" async></script> --}}
                            

@endsection

