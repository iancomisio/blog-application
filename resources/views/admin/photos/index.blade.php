@extends('layouts.admin')

@section('content')

    <h1>Photos</h1>

    @if($photos)

        <form action="delete/photos" method="POST" class="form-inline">
            @csrf
            @method('DELETE')
            <div class="form-group">
                <select name="checkBoxArray" id="" class="form-control">
                    <option value="">Delete</option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" name="delete_all" class="btn-primary form-control">
            </div>


            <table class="table">
                <thead>
                <tr>
                    <th><input type="checkbox" id="options"></th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Created</th>
                </tr>
                </thead>
                <tbody>

                @foreach($photos as $photo)

                    <tr>
                        <td><input class="checkBoxes" type="checkbox" name="checkBoxArray[]" value="{{$photo->id}}"></td>
                        <td>{{$photo->id}}</td>
                        <td><img height="50" src="{{$photo->file}}" alt=""></td>
                        <td>{{$photo->created_at ? $photo->created_at : 'no date' }}</td>
                        <td>
                            <input type="hidden" name="photo" value="{{$photo->id}}">
                            {{-- {!! Form::open(['method'=>'DELETE', 'action'=> ['AdminPhotosController@destroy', $photo->id]]) !!} --}}
                                {{-- <div class="form-group"> --}}
                                    {{-- {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!} --}}
                                   {{--  <input type="submit" name="delete_single" value="Delete" class="btn btn-danger">
                                </div> --}}
                            {{-- {!! Form::close() !!} --}}

                        </td>
                    </tr>

                @endforeach

                </tbody>
            </table>

            <div class="row">
                <div class="col-sm-6 col-sm-offset-5">

                    {{$photos->render()}}

                </div>
            </div>
        
        </form>

    @endif

@endsection

@section('scripts')

<script>
    $(document).ready(function(){
        
        $('#options').click(function(){
            if(this.checked){
                $('.checkBoxes').each(function(){
                    this.checked = true;
                });
            }
            else{
                $('.checkBoxes').each(function(){
                    this.checked = false;
                });
            }
        });
    }); 
</script>

@endsection