<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Role;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $roles = Role::all();
        if($roles->isEmpty()){
            Role::create(['name'=>'administrator']);
            Role::create(['name'=>'author']);
            Role::create(['name'=>'subscriber']);
        }
        
        // $year = Carbon::now()->year;
        $posts = Post::paginate(2);
        $categories = Category::all();
        return view('front/home', compact('posts', 'categories'));
    }

    public function post($slug)
    {
        $post = Post::findBySlugOrFail($slug);
        $comments = $post->comments()->whereIsActive(1)->get();   
        $categories = Category::all();
        return view('post', compact('post','comments','categories'));
    }

    public function profile()
    {
        $user = Auth::user();
        $roles = Role::pluck('name','id')->all();
        return view('admin.users.edit', compact('user','roles'));
    }

}
