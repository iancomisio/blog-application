<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRepliesCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\CommentReply;
use App\Comment;
use Session;
 
class AdminCommentRepliesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRepliesCreateRequest $request)
    {
        $user = Auth::user();

        $data = [
            'comment_id' => $request->comment_id,
            'author'=> $user->name,
            'email' =>$user->email,
            'photo'=>$user->photo ? $user->photo->file : '',
            'body'=>$request->body
        ];

        CommentReply::create($data);
        $request->session()->flash('success_reply_message','Your reply has been submitted and is waiting moderation');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $comment = Comment::findOrFail($id);
        $replies = $comment->replies()->paginate(5);
        return view(' admin.comments.replies.show', compact('replies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reply = CommentReply::findOrFail($id);
        $reply->update($request->all());

        if($reply->is_active == 1){
            $request->session()->flash('success_reply_approved_message','The reply has been approved successfully!');
        }
        elseif($reply->is_active == 0){
            $request->session()->flash('success_reply_unapproved_message','The reply has been un-approved successfully!');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CommentReply::findOrFail($id)->delete();
        Session::flash('success_delete_reply_message','The reply has been deleted successfully!');
        return redirect()->back();
    }

    // public function createReply(CommentRepliesCreateRequest $request){

    //     $user = Auth::user();

    //     $data = [
    //         'comment_id' => $request->comment_id,
    //         'author'=> $user->name,
    //         'email' =>$user->email,
    //         'photo'=>$user->photo ? $user->photo->file : '',
    //         'body'=>$request->body
    //     ];

    //     CommentReply::create($data);
    //     $request->session()->flash('success_comment_reply_message','Your reply has been submitted and is waiting moderation');
    //     return redirect()->back();
    // }

}
