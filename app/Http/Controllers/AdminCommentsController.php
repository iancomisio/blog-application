<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentsCreateRequest;
use Illuminate\Http\Request;
use App\Comment;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Session;
 
class AdminCommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::paginate(5);
        return view('admin.comments.index', compact('comments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentsCreateRequest $request)
    {
        $user = Auth::user();

        $data = [
            'post_id' => $request->post_id,
            'author'=> $user->name,
            'email' =>$user->email,
            'photo'=>$user->photo ? $user->photo->file : '',
            'body'=>$request->body
        ];

        Comment::create($data);
        $request->session()->flash('success_comment_message','Your message has been submitted and is waiting moderation');
        return redirect()->back();
    }

    /** 
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        $comments = $post->comments()->paginate(5);
        return view('admin.comments.show', compact('comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::findOrFail($id);
        $comment->update($request->all());
        
        if($comment->is_active == 1){
            $request->session()->flash('success_comment_approved_message','The comment has been approved successfully!');
        }
        elseif($comment->is_active == 0){
            $request->session()->flash('success_comment_unapproved_message','The comment has been un-approved successfully!');
        }
        return redirect('/admin/comments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comment::findOrFail($id)->delete();
        Session::flash('success_delete_comment_message','The comment has been deleted successfully!');
        return redirect()->back();
    }
}
