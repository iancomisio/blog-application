<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoriesCreateRequest;
use App\Http\Requests\CategoriesEditRequest;
use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Session;
class AdminCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(5);
        return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesCreateRequest $request)
    {
        Category::create($request->all());
        $posts = Post::all();
        $category = Category::all();

        if($posts->isEmpty() && $category->count() == 1){
            $request->session()->flash('success_create_category_message','A new category has been created successfully!');
            return redirect(route('posts.create'));
        }
        else{
            $request->session()->flash('success_create_category_message','A new category has been created successfully!');
            return redirect('/admin/categories'); 
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriesEditRequest $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->update($request->all());
        $request->session()->flash('success_update_category_message','The category has been updated successfully!');
        return redirect('/admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::findOrFail($id)->delete();
        Session::flash('success_delete_category_message','The category has been deleted successfully!');
        return redirect('/admin/categories');
    }
}
